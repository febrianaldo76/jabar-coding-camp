// Aldo Febrian

// Soal Satu

let kalimat1 = "saya sangat senang hari ini";
let kalimat2 = "belajar javascript itu keren";

// Jawaban Soal Satu

let saya = kalimat1.substr(0, 5);
let senang = kalimat1.substr(12, 7);
let belajar = kalimat2.substr(0, 8);
let javascript = kalimat2.substr(8, 11);
let hasil1 = saya.concat("", senang);
let hasil2 = belajar.concat("", javascript);
let hasilakhir = hasil1.concat("", hasil2);

console.log(hasilakhir);

// Soal Dua
var kataPertama = 10,
    kataKedua = 2,
    kataKetiga = 4,
    kataKeempat = 6;

// Jawaban Soal Dua

console.log(kataPertama + kataKeempat + kataKetiga * kataKedua);

//Soal Tiga

let kalimat = "wah javascript itu keren sekali";

// Jawaban Soal Tiga
let kataPertama = kalimat.substring(0, 3);
let kataKedua = kalimat.substring(4, 14);
let kataKetiga = kalimat.substring(15, 8);
let kataKeempat = kalimat.substring(19, 24);
let kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama:" + kataPertama);
console.log("Kata Kedua:" + kataKedua);
console.log("Kata Ketiga:" + kataKetiga);
console.log("Kata Keempat:" + kataKeempat);
console.log("Kata Kelima:" + kataKelima);